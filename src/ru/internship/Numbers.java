package ru.internship;

public class Numbers {
    private static final int INPUT_NUMBER = 20;

    public static int getNumber(int number) {
        StringBuilder stringBuilder = new StringBuilder();

        int newNumber = number;

        while (newNumber > 9) {
            for (int i = 9; i >= 1; i--) {
                if (i == 1) {
                    return 0;
                }

                if (newNumber % i == 0) {
                    stringBuilder.append(i);

                    newNumber /= i;
                    break;
                }
            }
        }

        stringBuilder.append(newNumber);

        return Integer.parseInt(stringBuilder.reverse().toString());
    }

    public static void main(String[] args) {
        System.out.println(getNumber(INPUT_NUMBER));
    }
}