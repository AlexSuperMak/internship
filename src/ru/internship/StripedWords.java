package ru.internship;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StripedWords {
    private static final String TEXT = "how, to, add, existing, repository, to, gitlab";

    public static int countWords(String text) {
        String[] wordsArray = text.split("\\W+");

        String regex = "[aeouiy]{2}|[bcdfghjklmnpqrstvwxz]{2}|\\b\\w\\b|[0-9]+";

        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        int stripedWordsCount = 0;

        for (String word : wordsArray) {
            boolean isStripedWord = true;

            Matcher matcher = pattern.matcher(word);

            if (matcher.find()) {
                isStripedWord = false;
            }

            if (isStripedWord) {
                stripedWordsCount++;
            }
        }

        return stripedWordsCount;
    }

    public static void main(String[] args) {
        System.out.println(countWords(TEXT));
    }
}