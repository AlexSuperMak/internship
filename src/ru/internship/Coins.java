package ru.internship;

import java.util.Arrays;

public class Coins {
    private static final int[] COINS = {1, 1, 5, 7};

    private static int getAllCoinsSum(int[] coins) {
        int allCoinsSum = 0;

        for (int coin : coins) {
            allCoinsSum += coin;
        }

        return allCoinsSum;
    }

    public static int getSum(int[] coins) {
        Arrays.sort(coins);

        if (coins[0] > 1) {
            return 1;
        }

        int allCoinsSum = getAllCoinsSum(coins);

        int currentCoinsSum = 0;

        for (int i = 1, j = 0; i < allCoinsSum && j < coins.length; i++) {
            if (coins[j] <= i) {
                currentCoinsSum += coins[j];
                j++;
                continue;
            }

            if (currentCoinsSum < i) {
                return i;
            }
        }

        return allCoinsSum + 1;
    }

    public static void main(String[] args) {
        System.out.println(getSum(COINS));
    }
}