package ru.internship;

public class SequenceInMatrix {
    private final static int[][] MATRIX = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 8, 1, 1, 1, 1},
            {1, 1, 1, 8, 1, 1, 1, 1, 1},
            {1, 1, 8, 1, 1, 1, 1, 1, 1},
            {1, 8, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1}};

    private final static int SEARCHED_NUMBER = 8;

    private static boolean hasHorizontalOrVerticalSequence(int number, int[][] matrix) {
        int[][] horizontalStateMatrix = new int[matrix.length][matrix.length];
        int[][] verticalStateMatrix = new int[matrix.length][matrix.length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 1; j < matrix.length; j++) {
                if (matrix[i][j] == number && matrix[i][j - 1] == number) {
                    horizontalStateMatrix[i][j] = horizontalStateMatrix[i][j - 1] + 1;
                }

                if (matrix[j][i] == number && matrix[j - 1][i] == number) {
                    verticalStateMatrix[j][i] = verticalStateMatrix[j - 1][i] + 1;
                }

                if (horizontalStateMatrix[i][j] == 3 || verticalStateMatrix[j][i] == 3) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean hasDiagonalSequence(int number, int[][] matrix) {
        int[][] diagonalStateMatrix = new int[matrix.length][matrix.length];
        int[][] reverseDiagonalStateMatrix = new int[matrix.length][matrix.length];

        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix.length; j++) {
                if (matrix[i][j] == number && matrix[i - 1][j - 1] == number) {
                    diagonalStateMatrix[i][j] = diagonalStateMatrix[i - 1][j - 1] + 1;
                }

                if (matrix[i][matrix.length - j - 1] == number && matrix[i - 1][matrix.length - j] == number) {
                    reverseDiagonalStateMatrix[i][matrix.length - j - 1] = reverseDiagonalStateMatrix[i - 1][matrix.length - j] + 1;
                }

                if (diagonalStateMatrix[i][j] == 3 || reverseDiagonalStateMatrix[i][matrix.length - j - 1] == 3) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean hasMatrixSequence(int number, int[][] matrix) {
        return hasHorizontalOrVerticalSequence(number, matrix) || hasDiagonalSequence(number, matrix);
    }

    public static void main(String[] args) {
        System.out.println(hasMatrixSequence(SEARCHED_NUMBER, MATRIX));
    }
}