package ru.internship;

public class PrimePalindrome {
    private static final int MAX_NUMBER = 105;
    private static final int INPUT_NUMBER = 7;

    private static boolean isPalindrome(int primeNumber) {
        String number = String.format("%d", primeNumber);

        for (int i = 0, j = number.length() - 1; i <= j; i++, j--) {
            char leftSymbol = number.charAt(i);
            char rightSymbol = number.charAt(j);

            if (leftSymbol != rightSymbol) {
                return false;
            }
        }

        return true;
    }

    public static int getPrimePalindrome(int number) {
        if (number <= 1) {
            return 2;
        }

        for (int currentNumber = number + 1; currentNumber <= MAX_NUMBER; currentNumber++) {
            boolean isPrimeNumber = true;

            double currentNumberSquareRoot = Math.sqrt(currentNumber);

            for (int j = 2; j <= currentNumberSquareRoot; j++) {
                if (currentNumber % j == 0) {
                    isPrimeNumber = false;
                    break;
                }
            }

            if (isPrimeNumber && isPalindrome(currentNumber)) {
                return currentNumber;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(getPrimePalindrome(INPUT_NUMBER));
    }
}
